"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_samples admin *

:details: lara_django_samples admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_samples >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_samples
# generated with django-extensions tests_generator  lara_django_samples > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, SampleLayout, SampleClass, Sample


class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_samples:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = ('data_type',
                  'namespace',
                  'URI',
                  'text',
                  'XML',
                  'JSON',
                  'bin',
                  'media_type',
                  'IRI',
                  'URL',
                  'description',

                  'file')


class SampleLayoutTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_samples:samplelayout-detail', [tables.A('pk')]))

    class Meta:
        model = SampleLayout

        fields = (
            'namespace',
            'name',
            'rows',
            'cols',
            'layout_bin',
            'layout_JSON',
            'layout_file',
            'vol_unit',
            'conc_unit',
            'description')


class SampleClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_samples:sampleclass-detail', [tables.A('pk')]))

    class Meta:
        model = SampleClass

        fields = (
            'name',
            'description')


class SampleTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_samples:sample-detail', [tables.A('pk')]))

    class Meta:
        model = Sample

        fields = (
            'namespace',
            'name',
            'name_full',
            'sample_class',
            'labware',
            'sample_layout',
            'URL',
            'handle',
            'IRI',
            'barcode',
            'date_created',
            'date_modified',
            'date_discarded',
            'data_expiry',
            'location',
            'substance',
            'solvent',
            'mixture',
            'polymer',
            'description')
