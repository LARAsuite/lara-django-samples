"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_samples admin *

:details: lara_django_samples admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_samples >> forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, SampleLayout, SampleClass, Sample


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = ('data_type',
                  'namespace',
                  'URI',
                  'text',
                  'XML',
                  'JSON',

                  'media_type',
                  'IRI',
                  'URL',
                  'description',

                  'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',

            'media_type',
            'IRI',
            'URL',
            'description',

            'file',
            Submit('submit', 'Create')
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = ('data_type',
                  'namespace',
                  'URI',
                  'text',
                  'XML',
                  'JSON',

                  'media_type',
                  'IRI',
                  'URL',
                  'description',

                  'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',

            'media_type',
            'IRI',
            'URL',
            'description',

            'file',
            Submit('submit', 'Create')
        )


class SampleLayoutCreateForm(forms.ModelForm):
    class Meta:
        model = SampleLayout
        fields = (
            'namespace',
            'name',
            'rows',
            'cols',
            'layout_bin',
            'layout_JSON',
            'layout_file',
            'vol_unit',
            'conc_unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'namespace',
            'name',
            'rows',
            'cols',
            'layout_bin',
            'layout_JSON',
            'layout_file',
            'vol_unit',
            'conc_unit',
            'description',
            Submit('submit', 'Create')
        )


class SampleLayoutUpdateForm(forms.ModelForm):
    class Meta:
        model = SampleLayout
        fields = (
            'namespace',
            'name',
            'rows',
            'cols',
            'layout_bin',
            'layout_JSON',
            'layout_file',
            'vol_unit',
            'conc_unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'namespace',
            'name',
            'rows',
            'cols',
            'layout_bin',
            'layout_JSON',
            'layout_file',
            'vol_unit',
            'conc_unit',
            'description',
            Submit('submit', 'Create')
        )


class SampleClassCreateForm(forms.ModelForm):
    class Meta:
        model = SampleClass
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'name',
            'description',
            Submit('submit', 'Create')
        )


class SampleClassUpdateForm(forms.ModelForm):
    class Meta:
        model = SampleClass
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'name',
            'description',
            Submit('submit', 'Create')
        )


class SampleCreateForm(forms.ModelForm):
    class Meta:
        model = Sample
        fields = (
            'namespace',
            'name',
            'name_full',
            'sample_class',
            'labware',
            'sample_layout',
            'URL',
            'handle',
            'IRI',
            'barcode',
            'date_created',
            'date_modified',
            'date_discarded',
            'data_expiry',
            'location',
            'substance',
            'solvent',
            'mixture',
            'polymer',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'namespace',
            'name',
            'name_full',
            'sample_class',
            'labware',
            'sample_layout',
            'URL',
            'handle',
            'IRI',
            'barcode',
            'date_created',
            'date_modified',
            'date_discarded',
            'data_expiry',
            'location',
            'substance',
            'solvent',
            'mixture',
            'polymer',
            'description',
            Submit('submit', 'Create')
        )


class SampleUpdateForm(forms.ModelForm):
    class Meta:
        model = Sample
        fields = (
            'namespace',
            'name',
            'name_full',
            'sample_class',
            'labware',
            'sample_layout',
            'URL',
            'handle',
            'IRI',
            'barcode',
            'date_created',
            'date_modified',
            'date_discarded',
            'data_expiry',
            'location',
            'substance',
            'solvent',
            'mixture',
            'polymer',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            'namespace',
            'name',
            'name_full',
            'sample_class',
            'labware',
            'sample_layout',
            'URL',
            'handle',
            'IRI',
            'barcode',
            'date_created',
            'date_modified',
            'date_discarded',
            'data_expiry',
            'location',
            'substance',
            'solvent',
            'mixture',
            'polymer',
            'description',
            Submit('submit', 'Create')
        )

# from .forms import ExtraDataCreateForm, SampleLayoutCreateForm, SampleClassCreateForm, SampleCreateFormExtraDataUpdateForm, SampleLayoutUpdateForm, SampleClassUpdateForm, SampleUpdateForm
