"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_samples admin *

:details: lara_django_samples admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

:date: (creation)       20230416

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_samples >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, SampleLayout, SampleClass, Sample


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(SampleLayout)
class SampleLayoutAdmin(admin.ModelAdmin):
    list_display = (
        'layout_id',
        'namespace',
        'name',
        'rows',
        'cols',
        'layout_bin',
        'layout_JSON',
        'layout_file',
        'vol_unit',
        'conc_unit',
        'description',
    )
    list_filter = ('namespace',)
    search_fields = ('name',)


@admin.register(SampleClass)
class SampleClassAdmin(admin.ModelAdmin):
    list_display = ('class_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(Sample)
class SampleAdmin(admin.ModelAdmin):
    list_display = (
        'sample_id',
        'namespace',
        'name',
        'name_full',
        'sample_class',
        'labware',
        'sample_layout',
        'URL',
        'handle',
        'IRI',
        'barcode',
        'date_created',
        'date_modified',
        'date_discarded',
        'data_expiry',
        'location',
        'substance',
        'solvent',
        'mixture',
        'polymer',
        'description',
    )
    list_filter = (
        'namespace',
        'sample_class',
        'labware',
        'sample_layout',
        'date_created',
        'date_modified',
        'date_discarded',
        'data_expiry',
        'location',
        'substance',
        'solvent',
        'mixture',
        'polymer',
    )
    raw_id_fields = ('users', 'organisms', 'parent_sample')
    search_fields = ('name',)
