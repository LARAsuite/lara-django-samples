"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_samples models *

:details: lara_django_samples database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""


import logging
import uuid

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Namespace, Tag, Location
from lara_django_people.models import Entity, LaraUser

from lara_django_material_store.models import LabwareInstance
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance
from lara_django_organisms_store.models import OrganismInstance

settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(
        upload_to='samples', blank=True, null=True, help_text="rel. path/filename")


class SampleLayout(models.Model):
    """ Labware sample layout information, e.g. description what individual wells in a microtiterplate contain """
    layout_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of sample layout")
    name = models.TextField(unique=True, null=True,
                            help_text="unique, fully qualified name of layout, containing namespace, e.g.: de.unigreifswald.biochem.akb.lara.2333_plate_layout_1.csv")
    rows = models.IntegerField(
        blank=True, null=True, help_text="number of rows")
    cols = models.IntegerField(
        blank=True, null=True, help_text="number of columns")
    layout_bin = models.BinaryField(
        blank=True, null=True, help_text="binary representation - e.g. for pickled layouts")
    layout_JSON = models.TextField(
        blank=True, null=True, help_text="JSON representation of layout")
    layout_file = models.FileField(upload_to='samples/layouts', blank=True, null=True,
                                   help_text="reference to the layout csv file")
    # should we always use SI units ?
    vol_unit = models.TextField(
        default="uL", null=True, help_text="volume unit")
    conc_unit = models.TextField(
        default="uM", null=True, help_text="concentration unit")
    description = models.TextField(
        blank=True, null=True, help_text="sample layout description")

    def __str__(self):
        return self.name or ''

    class Meta:
        """ Meta class """
        # db_table = "lara_labwares_labware_layout"


class SampleClass(models.Model):
    """ classes for sample, e.g. soil sample, tissue sample, reaction mixture, assay, ... """
    class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(unique=True,
                            help_text="name of the sample class, e.g. soil sample, tissue sample, reaction mixture, assay, ...")
    description = models.TextField(
        blank=True, help_text="sample class description ")

    def __str__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = "SampleClasses"


class Sample(models.Model):
    """Generic Sample class, can be used to represent any kind of sample, e.g. a (reaction) mixture, an assay, a soil sample, 

    :param models: _description_
    :type models: _type_
    """
    sample_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of sample")
    name = models.TextField(help_text="short part or device name")
    name_full = models.TextField(
        blank=True, help_text="long part or device name")
    sample_class = models.ForeignKey(SampleClass, related_name='%(app_label)s_%(class)s_sample_classes_related',
                                     related_query_name="%(app_label)s_%(class)s_sample_classes_related_query",
                                     on_delete=models.CASCADE, null=True, blank=True,
                                     help_text="sample class")

    labware = models.ForeignKey(LabwareInstance, related_name='%(app_label)s_%(class)s_labware_related',
                                related_query_name="%(app_label)s_%(class)s_labware_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="labware instance containing sample")
    sample_layout = models.ForeignKey(SampleLayout, related_name='%(app_label)s_%(class)s_sample_layouts_related',
                                      related_query_name="%(app_label)s_%(class)s_sample_layouts_related_query",
                                      on_delete=models.CASCADE, null=True, blank=True,
                                      help_text="sample layout of sample")
    URL = models.URLField(
        blank=True, help_text="Universal Resource Locator - URL")
    handle = models.URLField(
        blank=True, null=True,  unique=True, help_text="handle URI")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    barcode = models.TextField(blank=True, unique=True,
                               help_text="barcode of the general part type, not of a specific part !!")

    date_created = models.DateTimeField(
        default=timezone.now, help_text="sample creation date")
    date_modified = models.DateTimeField(
        auto_now=True, help_text="sample modification date")
    date_discarded = models.DateTimeField(
        blank=True, null=True, help_text="sample discarded date")
    data_expiry = models.DateTimeField(
        blank=True, null=True, help_text="sample expiry date")

    users = models.ManyToManyField(LaraUser, related_name='%(app_label)s_%(class)s_users_related',
                                   related_query_name="%(app_label)s_%(class)s_users_related_query",
                                   blank=True, help_text="users associated with sample, e.g. sample creator")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_locations_related',
                                 related_query_name="%(app_label)s_%(class)s_locations_related_query",
                                 on_delete=models.CASCADE, null=True, blank=True,
                                 help_text="location of sample")
    substance = models.ForeignKey(SubstanceInstance, related_name='%(app_label)s_%(class)s_substances_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="substance instance of sample")
    solvent = models.ForeignKey(MixtureInstance, related_name='%(app_label)s_%(class)s_solvents_related',
                                related_query_name="%(app_label)s_%(class)s_solvents_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="solvent instance of sample")
    mixture = models.ForeignKey(MixtureInstance, related_name='%(app_label)s_%(class)s_mixtures_related',
                                related_query_name="%(app_label)s_%(class)s_mixtures_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="mixture instance of sample, impurities can also be modeled as mixtures")
    polymer = models.ForeignKey(PolymerInstance, related_name='%(app_label)s_%(class)s_polymers_related',
                                related_query_name="%(app_label)s_%(class)s_polymers_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="polymer instance of sample")
    organisms = models.ManyToManyField(OrganismInstance, related_name='%(app_label)s_%(class)s_organisms_related',
                                       related_query_name="%(app_label)s_%(class)s_organisms_related_query",
                                       blank=True, help_text="organisms associated with sample")

    parent_sample = models.ManyToManyField('self',  # related_name='%(app_label)s_%(class)s_parent_samples_related',
                                           related_query_name="%(app_label)s_%(class)s_parent_samples_related_query",
                                           blank=True,
                                           help_text="parent sample of sample (provenance)")
    # Chemotion Compatibility
    # "real_amount_value"
    # "real_amount_unit"
    # "target_amount_value"
    # "target_amount_unit"
    # "stereo"
    # "metrics",
    # "decoupled",

    description = models.TextField(
        blank=True, null=True, help_text="sample description")

    def __str__(self):
        return f"{self.name}" or ""

    def __repr__(self):
        return f"{self.name}" or ""
