"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_samples tests *

:details: lara_django_samples application models tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

# from lara_django_samples.models import

# Create your lara_django_samples tests here.
