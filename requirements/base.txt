pytz==pytz-2023.3  # https://github.com/stub42/pytz
python-slugify==8.0.1  # https://github.com/un33k/python-slugify
Pillow==9.5.0  # https://github.com/python-pillow/Pillow


# Django
# ------------------------------------------------------------------------------
django==4.2  # pyup: < 4.1  # https://www.djangoproject.com/
django-environ==0.10.0  # https://github.com/joke2k/django-environ
django-model-utils==4.3.1  # https://github.com/jazzband/django-model-utils
django-allauth==0.54.0  # https://github.com/pennersr/django-allauth
django-crispy-forms==2.0  # https://github.com/django-crispy-forms/django-crispy-forms
django-bootstrap5==23.1
crispy-bootstrap5==0.7  # https://github.com/django-crispy-forms/crispy-bootstrap5

# Django REST Framework
djangorestframework==3.14.0  # https://github.com/encode/django-rest-framework
django-cors-headers==3.14.0  # https://github.com/adamchainz/django-cors-headers
# DRF-spectacular for api documentation
drf-spectacular==0.26.2  # https://github.com/tfranzel/drf-spectacular

